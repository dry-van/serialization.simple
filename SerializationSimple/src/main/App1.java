package main;

import java.io.Serializable;

public class App1 implements Serializable {
	private static final long serialVersionUID = -609806407919261640L;
	
	private static final String FIRST ="first";
	private transient String second ="second";
	
	private String something; 

	public App1(String something) {
		this.something = something;
	}

	@Override
	public String toString() {
		return "App1 [something=" + something + "]"+FIRST;
	}

	public  String getSecond() {
		return second;
	}
	

}
